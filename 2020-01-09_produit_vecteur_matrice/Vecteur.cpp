
#include "Vecteur.h"
#include "Matrice.h"
#include <iostream>

///Assesseurs
float Vecteur::getX() {
	return fltX;
}

float Vecteur::getY() {
	return fltY;
}

float Vecteur::getZ() {
	return fltZ;
}

///mutateurs
void Vecteur::setX(float fltX) {
	this->fltX = fltX;
}

void Vecteur::setY(float fltY) {
	this->fltY = fltY;
}

void Vecteur::setZ(float fltZ) {
	this->fltZ = fltZ;
}

void Vecteur::AfficherVecteur() {
	std::cout << "< " << this->getX() << ", " << this->getY() << ", " << this->getZ() << " >" << std::endl;
};

void Vecteur::AfficherVecteur(Vecteur &Vect) {
	//std::cout << "< " << Vect.fltX() << ", " << Vect.fltY() << " >" << std::endl;
}

//constructors
Vecteur::Vecteur() {
	this->fltX = 0;
	this->fltY = 0;
	this->fltZ = 0;
}

Vecteur::Vecteur(float fltX, float fltY, float fltZ) {
	this->fltX = fltX;
	this->fltY = fltY;
	this->fltZ = fltZ;
}

