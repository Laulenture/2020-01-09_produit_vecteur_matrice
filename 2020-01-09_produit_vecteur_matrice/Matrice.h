#pragma once

#include <iostream>

class Vecteur;

class Matrice
{
	private:
		float Mat[3][3] = { 0 };

	public:

		void AfficheMatrice();

		//Fonction independante amie de plusieurs classes
		friend void Produit1(const Vecteur &Vect, const Matrice& Mat);

		//Fonction membre de la classe Matrice amie de la classe Vecteur
		void Produit2(const Vecteur& Vect, const Matrice& Mat);

		//Constructors
		Matrice();
};
