
#include "Matrice.h"
#include "Vecteur.h"
#include <iostream>


void Matrice::AfficheMatrice()
{
    /*int i, j;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            std::cout << "< " << this->Mat[i, j] << " >";
        }
        std::cout << std::endl;
    }

	std::cout << std::endl;*/
}


//Fonction independante amie de plusieurs classes
void Produit1(const Vecteur &Vect, const Matrice &Mat) {
    Vecteur VectRes(
        (Mat.Mat[0][0] * Vect.fltX + Mat.Mat[0][1] * Vect.fltY + Mat.Mat[0][2] * Vect.fltZ),
        (Mat.Mat[1][0] * Vect.fltX + Mat.Mat[1][1] * Vect.fltY + Mat.Mat[1][2] * Vect.fltZ),
        (Mat.Mat[2][0] * Vect.fltX + Mat.Mat[2][1] * Vect.fltY + Mat.Mat[2][2] * Vect.fltZ)
    );
    VectRes.AfficherVecteur();
}

//Fonction membre de la classe Matrice amie de la classe Vecteur
void Matrice::Produit2(const Vecteur &Vect, const Matrice &Mat) {
    
}


//Constructors
Matrice::Matrice() {
    float val = 1;
    
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            this->Mat[i][j] = val;
            val++;
        }
    }
}