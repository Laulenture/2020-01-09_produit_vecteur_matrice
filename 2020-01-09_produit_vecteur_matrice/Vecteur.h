#pragma once

#include <iostream>
#include "Matrice.h"

class Vecteur {

private:
	///que dans la classe ell-meme
	float fltX, fltY, fltZ;	//pour les 3 compostantes (cartesiennes)

public:

	///Assesseurs
	float getX();
	float getY();
	float getZ();
	///Mutateurs
	void setX(float fltX);
	void setY(float fltY);
	void setZ(float fltZ);

	void AfficherVecteur();
	static void AfficherVecteur(Vecteur &Vect);
	//Fonction independante amie de plusieurs classes
	friend void Produit1(const Vecteur &Vect, const Matrice &Mat);

	//constructors
	Vecteur();

	Vecteur(float fltX, float fltY, float fltZ);

};
